# Prueba Técnica
OPCIÓN 1.

Crea un proyecto mobile en flutter que tenga un formulario para crear un usuario con Nombre, Apellido, fecha de nacimiento y agregar una dirección física a tu cuenta (puedes tener muchas direcciones por usuario). El flujo deberá tener 3 pantallas, al menos. Te recomendamos seguir buenas prácticas como control de errores, estados y todo lo que haga que tu código sea elegante. Debemos poder pintar los datos del usuario en cualquier momento de la aplicación.
## Assets


Decripción

El aplicativo fue desarrollado para la arquitectura Android, con Flutter 2.0 y GetX para su control.

1. Lista de Usuarios
- En esta página se valida si la ListaUsuarios cuenta con Objets a mostrar, de lo contrario nos indicara mediante una imagen y un mensaje que no se tienen datos de usuarios.
- Para generar el ingreso de la información es necesario oprimir el botón “add” el cual es un floatingActionButton el cual redirecciona al usuario a la pagina de registro 

`Widget _btnNuevoUsuario() {
  return SizedBox(
    width: 60,
    height: 60,
    child: FloatingActionButton(
      child: Icon(Icons.add),
      onPressed: () {
        Get.to(RegistroUsuario(), transition: Transition.fadeIn);
      },
    ),
  );
}`

Imagen:
https://gitlab.com/Jespalza/prueba_tecnica/-/blob/master/assets/1.jpeg

2. En la pagina de registro solicitara los 4 datos solicitados por la prueba técnica, los cuales son
Nombre (Requerida para el Registro),
Apellido (Requerida para el Registro),
Fecha Nacimiento (Requerida para el Registro),
Dirección Física (El usuario puede agregar multiplex direcciones, estas se guardan en una lista)
Imagen:
https://gitlab.com/Jespalza/prueba_tecnica/-/blob/master/assets/2.jpeg

3. Nuevamente en la página principal ( Lista de Usuarios) se podra acceder a la información digitada oprimiendo el botón izquierdo ` icon: Icon(Icons.remove_red_eye_outlined),` el cual redireccionara a la tercera página (InfoUsuario)

Imagen:
https://gitlab.com/Jespalza/prueba_tecnica/-/blob/master/assets/4.jpeg

4. En esta pagina se podrá observar la información digitada y solo tiene la opción de eliminar el usuario no de editarlo.

Imagen:
https://gitlab.com/Jespalza/prueba_tecnica/-/blob/master/assets/4.jpeg

