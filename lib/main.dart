import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:proyecto_1/src/controllers/global_controller.dart';
import 'package:proyecto_1/src/pages/Lista_usuarios.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Get.put(GlobalController());
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Prueba técnica',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: ListaUsuarios(),
    );
  }
}
