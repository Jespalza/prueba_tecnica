import 'package:meta/meta.dart' show required;

class Usuario {
  final String Nombre;
  final String Apellido;
  final String FechaNacimiento;
  final String direcciones;

  Usuario({
    required this.Nombre,
    required this.Apellido,
    required this.FechaNacimiento,
    required this.direcciones,
  });
}
