import 'package:flutter/material.dart';

class NuevaDireccion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextField(
        decoration: InputDecoration(
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
            hintText: 'Apellido',
            labelText: 'Apellido',
            suffixIcon: Icon(Icons.person_outline)),
      ),
    );
  }
}
