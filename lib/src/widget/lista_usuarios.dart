import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:proyecto_1/src/controllers/global_controller.dart';
import 'package:proyecto_1/src/pages/info_usuario.dart';

class WListaUsuarios extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<GlobalController>(builder: (_) {
      if (_.ListaUsuarios.length == 0) {
        return Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Sin datos de usuarios.',
                style: TextStyle(color: Colors.green, fontSize: 25),
              ),
              Image(
                  image:
                      AssetImage('assets/undraw_design_objectives_fy1r.png')),
            ],
          ),
        );
      }
      return ListView.builder(
        itemCount: _.ListaUsuarios.length,
        itemBuilder: (__, index) {
          return ListTile(
            leading: Icon(
              Icons.person_pin_rounded,
              size: 50,
              color: Colors.green[200],
            ),
            title: Text(
              _.ListaUsuarios[index]['Nombre'].toString(),
              style: TextStyle(fontSize: 20),
            ),
            subtitle: Text("${_.ListaUsuarios[index]['Apellido']}"),
            trailing: IconButton(
              onPressed: () {
                Get.to(
                  InfoUsuario(),
                  arguments: {
                    "index": index,
                    "usuario": _.ListaUsuarios[index]
                  },
                );
              },
              icon: Icon(Icons.remove_red_eye_outlined),
              color: Colors.green,
            ),
          );
        },
      );
    });
  }
}
