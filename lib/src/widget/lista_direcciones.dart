import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:proyecto_1/src/controllers/registro_usuario.dart';

class ListaDirecciones extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<RegistroUsuarioController>(
        builder: (_) => Container(
              child: Expanded(
                child: Column(
                  children: [
                    btnDireccion(_),
                    Obx(
                      () => Flexible(
                        child: Container(
                          child: _LVdireccion(_),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  Widget btnDireccion(_) {
    return MaterialButton(
      color: Colors.green,
      textColor: Colors.white,
      child: Text('Nueva Dirección'),
      onPressed: () {
        Get.defaultDialog(
          title: 'Nueva Dirección',
          content: Column(
            children: [
              TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20.0)),
                  hintText: 'Dirección',
                  suffixIcon: Icon(Icons.share_outlined),
                ),
                onChanged: (value) => _.direcion.value = value,
              ),
            ],
          ),
          confirm: RaisedButton(
            onPressed: () {
              _.nuevaDireccion();
              Get.back();
            },
            child: Text(
              "Guardar",
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.green,
          ),
        );
      },
    );
  }
}

Widget _LVdireccion(_) {
  return ListView.builder(
    shrinkWrap: true,
    itemCount: _.direcciones.length,
    itemBuilder: (__, i) => Card(
      child: Column(
        children: [
          ListTile(
            leading: Icon(Icons.home),
            title: Text(_.direcciones[i]),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              IconButton(
                color: Colors.red,
                icon: Icon(Icons.delete_forever_outlined),
                onPressed: () {
                  _.borrarDireccion(i);
                },
              ),
            ],
          )
        ],
      ),
    ),
  );
}
