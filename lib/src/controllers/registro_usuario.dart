import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:proyecto_1/src/controllers/global_controller.dart';
import 'package:proyecto_1/src/pages/Lista_usuarios.dart';

class RegistroUsuarioController extends GetxController {
  final global = Get.find<GlobalController>();
  RxString Nombre = ''.obs;
  RxString Apellido = ''.obs;
  RxString Fecha = ''.obs;
  RxString direcion = ''.obs;
  RxList<dynamic> direcciones = [].obs;

  @override
  void onInit() {
    super.onInit();
  }

  NuevoUsuario() {
    var json = {
      "Nombre": Nombre,
      "Apellido": Apellido,
      "Fecha": Fecha,
      "direcciones": direcciones
    };
    if (Nombre == '' || Apellido == '' || Fecha == '') {
      Get.defaultDialog(
          title: 'Error',
          titleStyle: TextStyle(color: Colors.red, fontSize: 25),
          content: Text(
              'Los datos Nombre, Apellido y Fecha de Nacimiento son obligatorios.'));
    } else {
      global.ListaUsuarios.add(json);
      print(global.ListaUsuarios);
      Get.off(ListaUsuarios());
    }
  }

  EliminarUsuario(index) {
    global.ListaUsuarios.removeAt(index);
  }

  nuevaDireccion() {
    if (direcion != '') {
      direcciones.add(direcion.value);
    }
  }

  borrarDireccion(int index) {
    direcciones.removeAt(index);
  }
}
