import 'package:get/get.dart';

class InfoUsuarioController extends GetxController {
  Map<dynamic, dynamic> info = {};
  int index = 0;

  @override
  void onInit() {
    super.onInit();
    info = Get.arguments['usuario'];
    index = Get.arguments['index'];
    print(info);
  }
}
