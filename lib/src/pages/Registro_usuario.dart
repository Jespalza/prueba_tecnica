import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:proyecto_1/src/controllers/registro_usuario.dart';
import 'package:proyecto_1/src/widget/lista_direcciones.dart';

class RegistroUsuario extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<RegistroUsuarioController>(
        init: RegistroUsuarioController(),
        builder: (_) {
          _.Fecha.value = '';
          // final date = DateFormat('MM/dd/yyy').format(_.Fecha.value);
          return Scaffold(
            appBar: AppBar(
              title: Text('Registro de usuario'),
            ),
            body: Container(
              margin: EdgeInsets.all(15),
              child: Column(
                children: [
                  Text(
                    'Datos requerido *',
                    style: TextStyle(color: Colors.red),
                  ),
                  SizedBox(height: 15),
                  _txtnombre(_),
                  SizedBox(height: 15),
                  _txtapellido(_),
                  SizedBox(
                    height: 15,
                  ),
                  _txtfecha(_),
                  ListaDirecciones()
                ],
              ),
            ),
            floatingActionButton: _btnGuardarUsuario(_),
          );
        });
  }
}

Widget _txtnombre(_) {
  return TextField(
    decoration: InputDecoration(
      border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
      hintText: 'Nombre *',
      labelText: 'Nombre *',
      suffixIcon: Icon(Icons.person),
    ),
    onChanged: (value) => _.Nombre.value = value,
  );
}

Widget _txtapellido(_) {
  return TextField(
    decoration: InputDecoration(
      border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
      hintText: 'Apellido *',
      labelText: 'Apellido *',
      suffixIcon: Icon(Icons.person_outline),
    ),
    onChanged: (value) => _.Apellido.value = value,
  );
}

Widget _txtfecha(_) {
  return Obx(
    () => TextField(
      controller: TextEditingController(text: _.Fecha.value),
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
        hintText: _.Fecha.value,
        labelText: 'Fecha Nacimiento *',
        suffixIcon: Icon(Icons.calendar_today),
      ),
      showCursor: true,
      readOnly: true,
      onTap: () {
        Get.bottomSheet(Container(
          height: 300,
          color: Colors.white,
          child: DatePicket(_),
        ));
      },
    ),
  );
}

Widget DatePicket(_) {
  return Column(
    children: [
      SizedBox(
        height: 280,
        child: CupertinoDatePicker(
          maximumYear: 2021,
          initialDateTime: DateTime.now(),
          mode: CupertinoDatePickerMode.date,
          onDateTimeChanged: (datetime) =>
              _.Fecha.value = DateFormat('MM/dd/yyy').format(datetime),
        ),
      ),
    ],
  );
}

Widget _btnGuardarUsuario(_) {
  return SizedBox(
    width: 80,
    height: 80,
    child: FloatingActionButton(
      child: Icon(Icons.save),
      onPressed: () {
        _.NuevoUsuario();
      },
    ),
  );
}
