import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:proyecto_1/src/controllers/global_controller.dart';
import 'package:proyecto_1/src/controllers/info_controller.dart';
import 'package:proyecto_1/src/pages/Lista_usuarios.dart';

class InfoUsuario extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _global = Get.find<GlobalController>();
    return GetBuilder<InfoUsuarioController>(
        init: InfoUsuarioController(),
        builder: (_) => Scaffold(
              appBar: AppBar(
                title: Text('Información de Usuario'),
                actions: [
                  IconButton(
                      onPressed: () {
                        _global.borrarUsuarios(_.index);
                        Get.offAll(ListaUsuarios());
                      },
                      icon: Icon(Icons.delete))
                ],
              ),
              body: Center(
                child: Container(
                  child: Column(
                    children: [
                      Stack(
                        children: [_crearFondo(), _info(_)],
                      ),
                      _LVdireccion(_)
                    ],
                  ),
                ),
              ),
            ));
  }
}

Widget _crearFondo() {
  return Container(
    margin: const EdgeInsets.only(bottom: 40.0),
    height: Get.height * 0.50,
    width: double.infinity,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.only(
        bottomLeft: Radius.circular(40),
        bottomRight: Radius.circular(40),
      ),
      gradient: LinearGradient(
        colors: <Color>[
          Color.fromRGBO(0, 124, 53, 1.0),
          Color.fromRGBO(45, 255, 103, 1.0)
        ],
      ),
    ),
  );
}

Widget _info(_) {
  return Expanded(
    child: Center(
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Icon(
            Icons.person_pin,
            color: Colors.white,
            size: 200,
          ),
          SizedBox(
            height: 10,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                _.info['Nombre'].toString(),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 25,
                ),
              ),
              Text(
                _.info['Apellido'].toString(),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 25,
                ),
              ),
              Text(
                _.info['Fecha'].toString(),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 25,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    ),
  );
}

Widget _LVdireccion(_) {
  return ListView.builder(
    shrinkWrap: true,
    itemCount: _.info['direcciones'].length,
    itemBuilder: (__, i) => Card(
      child: Column(
        children: [
          ListTile(
            leading: Icon(Icons.home),
            title: Text(_.info['direcciones'][i]),
          ),
        ],
      ),
    ),
  );
}
