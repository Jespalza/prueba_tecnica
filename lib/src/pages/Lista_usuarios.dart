import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:proyecto_1/src/controllers/global_controller.dart';
import 'package:proyecto_1/src/pages/Registro_usuario.dart';
import 'package:proyecto_1/src/widget/lista_usuarios.dart';

class ListaUsuarios extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<GlobalController>(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Lista Usuarios'),
        ),
        body: WListaUsuarios(),
        backgroundColor: Colors.white,
        floatingActionButton: _btnNuevoUsuario(),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      );
    });
  }
}

Widget _btnNuevoUsuario() {
  return SizedBox(
    width: 60,
    height: 60,
    child: FloatingActionButton(
      child: Icon(Icons.add),
      onPressed: () {
        Get.to(RegistroUsuario(), transition: Transition.fadeIn);
      },
    ),
  );
}
